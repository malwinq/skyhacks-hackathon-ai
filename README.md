# SkyHacks 2019

## Description

The main goal of this task is to automate the most time consuming process of mortgages – evaluation of a loan collateral. Usually, in case of mortgages, the collateral is some kind of real estate. As you may guess, the major factor in determining the value of property is its state and technical condition. During this task you will have to deal with the problem of automatic interior state detections, as well as the technical condition detection.

## Dataset

Dataset consists of multiple public photos of both the interior and exterior of apartments and houses. Dataset is divided into separate folders, each folder contains between …. and … photos belonging to a single decision class.

## Tasks

Task 1 (40% of total score):
Having the same set of pictures, your task is to find objects of interests among these. For example, we ask you to find all the chairs or bathtubs. The closed set of labels (one label per line) is given in task1_labels.txt file.

Task 2 (40% of total score):
Having the set of classes (one class per line, file: task2_classes.txt), your task is to predict what kind of room type (or generally speaking – picture class) it belongs to. You may approach this task in multiple ways: as a single-label object detection problem (find exactly one room type per picture) or as a decision problem (having the output of Task 1, predict the class by looking at objects inside the room).

Task 3 (20% of total score):
Having the set of pictures, your goal is to predict interior state, as well as the technical condition of a room/apartment.

## Prerequisites

We used Keras with TensorFlow backend to generate neural networks.

For data preparation we used OpenCV framework.

We collaborated with teammates and made learning process fast using GPUs from Google Colab.

## Authors

* **Malwina Kubas** 
* **Hanna Klimczak** 
* **Klaudia Palak** 