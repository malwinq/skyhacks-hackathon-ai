from keras import layers
from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.models import model_from_json
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

# neural network
model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(224, 224, 1)))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(256, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Flatten())
model.add(layers.Dropout(0.2))
model.add(layers.Dense(512, activation='relu'))
model.add(layers.Dense(6, activation='sigmoid'))

model.summary()

optim = optimizers.Adam()
model.compile(loss='categorical_crossentropy', optimizer=optim, metrics=['acc'])

df = pd.read_csv('/content/drive/My Drive/csv/Task2.csv', sep=';', header=0, names=['filename', 'task2_class'])
df_val = pd.read_csv('/content/drive/My Drive/csv/Task2_val.csv', sep=';', header=0, names=['filename', 'task2_class'])

# preprocessing of images
train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    rotation_range=20,
    width_shift_range=0.1,
    height_shift_range=0.1,
    shear_range=0.1,
    zoom_range=0.1,
    horizontal_flip=True)

test_datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = train_datagen.flow_from_dataframe(
    dataframe=df,
    directory=r'/content/drive/My Drive/task2_2/train',
    x_col="filename",
    y_col="task2_class",
    target_size=(224, 224),
    batch_size=32,
    class_mode='categorical',
    color_mode='grayscale')

validation_generator = test_datagen.flow_from_dataframe(
    dataframe=df_val,
    directory=r'/content/drive/My Drive/task2_2/test',
    x_col="filename",
    y_col="task2_class",
    target_size=(224, 224),
    batch_size=32,
    class_mode='categorical',
    color_mode='grayscale')

history = model.fit_generator(
    train_generator,
    steps_per_epoch=35,
    epochs=100,
    validation_data=validation_generator)

acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(len(acc))

plt.plot(epochs, acc, 'bo', label='Train accuracy')
plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
plt.title('The accuracy of training and validation')
plt.legend()

plt.figure()

plt.plot(epochs, loss, 'bo', label='Train loss')
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('The loss of training and validation')
plt.legend()

model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)

model.save_weights("model.h5")
print("Saved model to disk")