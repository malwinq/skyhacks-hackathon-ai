from keras import layers
from keras import models
from keras import optimizers
from keras_preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from keras.preprocessing.image import load_img
from IPython.display import display
from PIL import Image
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import save_img

# neural network
model = models.Sequential()
model.add(layers.Conv2D(32, (3, 3), activation='relu', input_shape=(100, 100, 1)))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(64, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Conv2D(128, (3, 3), activation='relu'))
model.add(layers.MaxPooling2D((2, 2)))
model.add(layers.Flatten())
model.add(layers.Dropout(0.2))
model.add(layers.Dense(512, activation='relu'))
model.add(layers.Dense(53, activation='sigmoid'))

model.summary()

model.compile(loss='categorical_crossentropy', optimizer=optimizers.RMSprop(lr=1e-3), metrics=['acc'])

# df = pd.read_csv('/content/drive/My Drive/hackathon/csv/Task1.csv', sep=';')
# df_val = pd.read_csv('/content/drive/My Drive/hackathon/csv/Task1_val.csv', sep=';')

columns = ['Bathroom',
           'Bathroom cabinet',
           'Bathroom sink',
           'Bathtub',
           'Bed',
           'Bed frame',
           'Bed sheet',
           'Bedroom',
           'Cabinetry',
           'Ceiling',
           'Chair',
           'Chandelier',
           'Chest of drawers',
           'Coffee table',
           'Couch',
           'Countertop',
           'Cupboard',
           'Curtain',
           'Dining room',
           'Door',
           'Drawer',
           'Facade',
           'Fireplace',
           'Floor',
           'Furniture',
           'Grass',
           'Hardwood',
           'House',
           'Kitchen',
           'Kitchen & dining room table',
           'Kitchen stove',
           'Living room',
           'Mattress',
           'Nightstand',
           'Plumbing fixture',
           'Property',
           'Real estate',
           'Refrigerator',
           'Roof',
           'Room',
           'Rural area',
           'Shower',
           'Sink',
           'Sky',
           'Table',
           'Tablecloth',
           'Tap',
           'Tile',
           'Toilet',
           'Tree',
           'Urban area',
           'Wall',
           'Window']

# x = []
# for index, row in df.iterrows():
#     image = load_img('/content/drive/My Drive/hackathon/reshape/{}'.format(row['filename']), grayscale=True)
#     img_array = img_to_array(image)
#     x.append(img_array)
#     print(index)


# df.drop(df.columns[[0]], axis=1, inplace=True)
# print(df)

# x_val = []
# for index_val, row_val in df_val.iterrows():
#     image = load_img('/content/drive/My Drive/hackathon/reshape_val/{}'.format(row_val['filename']), grayscale=True)
#     img_array = img_to_array(image)
#     x_val.append(img_array)
#     print(index)

# df_val.drop(df_val.columns[[0]], axis=1, inplace=True)
# print(df_val)

x = np.array(x)
x_val = np.array(x_val)
"""
np.savetxt('DANE.txt', x)
np.savetxt('DANE_VAL.txt', x_val)

x = np.loadtxt('DANE.txt')
x_val = np.loadtxt('DANE_VAL.txt')
"""
val_data = (x_val, df_val)

history = model.fit(
    x,
    y=df,
    steps_per_epoch=100,
    epochs=30,
    validation_data=val_data,
    validation_steps=50)

acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(len(acc))

plt.plot(epochs, acc, 'bo', label='Train accuracy')
plt.plot(epochs, val_acc, 'b', label='Validation accuracy')
plt.title('The accuracy of training and validation')
plt.legend()

plt.figure()

plt.plot(epochs, loss, 'bo', label='Train loss')
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('The loss of training and validation')
plt.legend()