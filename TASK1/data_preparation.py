import numpy as np
import pandas as pd

df = pd.read_csv('/content/drive/My Drive/hackathon/csv/Task1.csv', sep=';')
df_val = pd.read_csv('/content/drive/My Drive/hackathon/csv/Task1_val.csv', sep=';')

columns = ['Bathroom',
           'Bathroom cabinet',
           'Bathroom sink',
           'Bathtub',
           'Bed',
           'Bed frame',
           'Bed sheet',
           'Bedroom',
           'Cabinetry',
           'Ceiling',
           'Chair',
           'Chandelier',
           'Chest of drawers',
           'Coffee table',
           'Couch',
           'Countertop',
           'Cupboard',
           'Curtain',
           'Dining room',
           'Door',
           'Drawer',
           'Facade',
           'Fireplace',
           'Floor',
           'Furniture',
           'Grass',
           'Hardwood',
           'House',
           'Kitchen',
           'Kitchen & dining room table',
           'Kitchen stove',
           'Living room',
           'Mattress',
           'Nightstand',
           'Plumbing fixture',
           'Property',
           'Real estate',
           'Refrigerator',
           'Roof',
           'Room',
           'Rural area',
           'Shower',
           'Sink',
           'Sky',
           'Table',
           'Tablecloth',
           'Tap',
           'Tile',
           'Toilet',
           'Tree',
           'Urban area',
           'Wall',
           'Window']

x = []
for index, row in df.iterrows():
    image = load_img('/content/drive/My Drive/hackathon/opencv/{}'.format(row['filename']))
    img_array = img_to_array(image)
    x.append(img_array)
    print(index)

df.drop(df.columns[[0]], axis=1, inplace=True)
print(df)

x_val = []
for index_val, row_val in df_val.iterrows():
    image = load_img('/content/drive/My Drive/hackathon/opencv_val/{}'.format(row['filename']))
    img_array = img_to_array(image)
    x_val.append(img_array)
    print(index)

df_val.drop(df.columns[[0]], axis=1, inplace=True)
print(df_val)

x = np.array(x)
x_val = np.array(x_val)

np.savetxt('DANE.txt', x)
np.savetxt('DANE_VAL.txt', x_val)